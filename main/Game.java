package main;

import static misc.Constants.VERSION;

import java.awt.AWTEvent;
import java.awt.Frame;
import java.awt.event.WindowEvent;

public class Game extends Frame {

	private static final long serialVersionUID = 1L;
	public static void main(String[] args) {
		new Game();
	}
	private Game() {
		super("SkyJump v" + VERSION);

		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		setResizable(false);

		final SJGame game = new SJGame();
		add(game);
		game.startGame();
		
		pack();
		setVisible(true);
	}
	public void processWindowEvent(WindowEvent e) {
		if (e.getID() == WindowEvent.WINDOW_CLOSING) System.exit(0);
	}
}