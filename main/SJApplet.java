package main;

import java.applet.Applet;
import java.awt.BorderLayout;

public class SJApplet extends Applet {

	private static final long serialVersionUID = 1L;
	
	private SJGame game;
	
	public void init() {
		setLayout(new BorderLayout());
		game = new SJGame();
		add(game, BorderLayout.CENTER);
	}
	public void start() {
		game.startGame();
	}
	public void stop() {
		game.stopGame();
	}
}
