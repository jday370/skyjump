package main;

import static misc.Constants.SIZE;
import static misc.Constants.START_BLOCKS;
import static misc.Methods.randInt;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import objects.Block;
import objects.FragileBlock;
import objects.Circle;
import objects.Circle.Form;
import objects.Lava;
import scenary.ScenaryHandler;

public class SJGame extends Canvas implements Runnable, MouseListener, KeyListener {
	//TODO lava rising and once you reach a certain level, level up?! upgrades. <<!!!
	//TODO no start menu just grass and press up to start <<!!!
	// in grass there is a button choice for lava survival or free jump or fragile blocks only
	private boolean running = false;

	private static final long serialVersionUID = 1L;

	private Image image;
	private Graphics dbGraphics;

	private static Circle c;
	private static Lava lava;

	private static final List<Block> b = new ArrayList<Block>();
	private static boolean hideBlocks = false;

	public static double height = 0D, cash = 0D, highscore = 0D;

	private static State state;
	enum State {
		START_MENU, GAME_ON, GAME_OVER, GAME_PAUSED, 
	}

	SJGame() {
		setSize(SIZE.width, SIZE.height);

		addMouseListener(this);
		addKeyListener(this);

		state = State.START_MENU;
	}
	public void startGame() {
		c = new Circle();
		lava = new Lava();

		b.add(new Block((int)c.getX()-c.getDiameter(), (int)(c.getY()+200)+c.getDiameter()-1));
		spawnBlocks(START_BLOCKS, true);

		state = State.GAME_ON;

		running = true;
		new Thread(this).start();
	}
	public void stopGame() {
		running = false;
	}
	public static void reset() {
		if (cash > highscore) highscore = cash;
		cash = 0;
		height = 0;
		c.reset();
		lava.reset();
		ScenaryHandler.reset();
		b.clear();
		b.add(new Block((int)c.getX()-c.getDiameter(), (int)(c.getY()+200)+c.getDiameter()-1));
		spawnBlocks(START_BLOCKS, true);
	}
	private static void spawnBlocks(final int amount, final boolean onScreen) {
		if (onScreen)
			for (int i=0; i<amount; i++)
				spawnBlock(randInt(0, SIZE.width-Block.getWidth()), randInt(0, SIZE.height-Block.getHeight()), false);
		else
			for (int i=0; i<amount; i++) {
				spawnBlock(randInt(0, SIZE.width-Block.getWidth()), 0, false);
			}
	}
	public static void checkHit(Circle circle) {
		boolean giveCash = true;
		for (Block block : b)
			if(block.intersects((int)circle.getX(), (int)circle.getY()+circle.getRadius(), circle.getDiameter(), circle.getDiameter())) {
				if (circle.falling) {
					if (circle.ghost) return;
					if (!block.touched) {
						if (giveCash)
							cash += 25;
						if (block instanceof FragileBlock) {
							b.remove(block);
							spawnNewBlock();
						} else  {
							block.touched = true;
						}
					}
					if (giveCash)
						circle.jump();
					return;
				}
			}
	}
	public void run() {
		try {
			while(running) {
				if (state == State.GAME_ON) {
					if (!hasFocus()) state = State.GAME_PAUSED;

					if (c.getY() < SIZE.height/4-c.getRadius() && !c.falling) {
						for (int i=0; i<b.size(); i++) {
							b.get(i).moveDown((int)c.getYVelocity());
							if (b.get(i).getY() >= SIZE.height) {
								b.remove(b.get(i));
								i--;
								spawnNewBlock();
								// TODO chance of spawning items
							}
						}
						// Move down all objects by (-c.getYVelocity)
						c.teleTo(c.getX(), SIZE.height/4-c.getRadius());
						lava.moveDown((int)c.getYVelocity());
						ScenaryHandler.moveDown((int)c.getYVelocity());
						cash += -c.getYVelocity()/3;
						height += -c.getYVelocity()/25;
					}
					c.process();
					ScenaryHandler.process(height);
					if(lava.isOnScreen())
						lava.moveUp(3);
					else 
						lava.moveUp(5);
					if (lava.intersects((int)c.getX(), (int)c.getY(), c.getDiameter(), c.getDiameter())) {
						c.hp -= 3;
						if (c.hp <= 0)
							reset();
						c.gravity /= 2;
					} else if (c.gravity != Circle.NORMAL_GRAVITY) {
						c.gravity = Circle.NORMAL_GRAVITY;
					}
					// if there is grass, then jump on it like a block else reset
					if (c.getY() >= SIZE.height) reset();
					if (c.getX() < -c.getDiameter()) c.setX(SIZE.width);
					if (c.getX() > SIZE.width) c.setX(-c.getDiameter());
				}
				repaint();
				Thread.sleep(50);
			}
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	private static void spawnNewBlock() {
		// TODO find how to get 89.. debug in circle :?
		final Block hBlock = getHighestBlock();
		if (hBlock.getY() > 100) {
			reset();
			highscore = -1;
		}
		spawnBlock(randInt(-Block.getWidth()/2, SIZE.width-Block.getWidth()/2), 
				hBlock.getY()-randInt(hBlock.getY()+Block.getHeight(), Circle.getMaxJumpHeight()), true);
	}
	private static void spawnBlock(final int x, final int y, final boolean lowerAmount) {
		if (lowerAmount && randInt(0, 10) == 0) return;
		if (randInt(0, 10) == 0)
			b.add(new FragileBlock(x, y));
		else
			b.add(new Block(x, y));
	}
	private static Block getHighestBlock() {
		int[] y = new int[b.size()];
		for (int i=0; i<b.size(); i++) {
			y[i] = b.get(i).getY();
		}
		for (int i=0; i<y.length; i++)
			for (int j=0; j<y.length; j++)
				if (i != j && y[i] > y[j]) {
					y[i] = y[j];
				}

		for (Block block : b) {
			if (block.getY() == y[0]) return block;
		}
		return null;
	}
	public void update(final Graphics g) {
		if (image == null) {
			image = createImage(SIZE.width, SIZE.height);
			dbGraphics = image.getGraphics();
		}
		dbGraphics.setColor(getBackground());
		dbGraphics.fillRect(0, 0, SIZE.width, SIZE.height);

		dbGraphics.setColor(getForeground());
		paint(dbGraphics);
		g.drawImage(image, 0, 0, this);
	}
	public void paint(final Graphics g) {
		switch (state) {
		case START_MENU:
			break;
		case GAME_ON:
			int colour = height > 255?0
					:255-(int)height;
			// TODO when colour gets black completely, stars spawn which move down 
			// half? the speed of velocity
			g.setColor(new Color(0, colour, colour));
			g.fillRect(0, 0, SIZE.width, SIZE.height);
			ScenaryHandler.paint(g);
			lava.paint(g);
			for(Block block : b) {
				if (!hideBlocks)
					block.paint(g);
			}
			colour = (int) (height > 255?255:height);
			g.setColor(new Color(colour, colour, colour));
			g.setFont(new Font("Arial", 0, 18));
			if (highscore == -1)
				g.drawString("High Score: Champion", 10, 20); // TODO no more highscore
			else
				g.drawString("High Score: " + (int)Math.round(highscore), 10, 20);//temp move coords to cash
			g.drawString("Cash: $" + (int)Math.round(cash), 10, 40);
			g.setColor(Color.black);
			g.fillRect(SIZE.width-120, 5, 115, 20);
			g.setColor(Color.white);
			g.drawRect(SIZE.width-115, 10, 50, 10);
			g.drawRect(SIZE.width-60, 10, 50, 10);
			g.setColor(Color.red);
			if (c.hp > 0) {
				int length = (int)(c.hp/2D);
				if (length > 49) length = 49;
				g.fillRect(SIZE.width-114, 11, length, 9);
			}
			g.setColor(Color.yellow);
			if (c.power > 0) {
				int length = (int)(c.power/2D);
				if (length > 49) length = 49;
				g.fillRect(SIZE.width-59, 11, length, 9);
			}

			c.paint(g);
			break;
		case GAME_PAUSED:
			// Paused Game Screen
			g.fillRect(0, 0, SIZE.width, SIZE.height);
			g.setColor(Color.white);
			String msg = "Game Paused";
			g.setFont(new Font("Arial", 2, 42));
			g.drawString(msg, SIZE.width/2-130, SIZE.height/2);
			g.setFont(new Font("Arial", 0, 12));
			if(!hasFocus())
				g.drawString("The game is not focused.", SIZE.width-145, 15);
			g.drawString("Press 'P' or double-click to unpause.", SIZE.width/2-95, SIZE.height/2+25);
			g.setColor(Color.black);
			break;
		}
	}
	public static boolean left = false, right = false;
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			left = true;
			break;
		case KeyEvent.VK_RIGHT:
			right = true;
			break;
		case KeyEvent.VK_P:
			if (state == State.GAME_PAUSED)
				state = State.GAME_ON;
			else
				state = State.GAME_PAUSED;
			break;
		case KeyEvent.VK_F:
			if (c.power == 100)
				c.toggleForm(Form.MEGA);
			break;
		case KeyEvent.VK_SPACE:
			if (c.power > 0)
				c.changeForm(Form.FLIGHT);
			break;
		}
	}
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			left = false;
			break;
		case KeyEvent.VK_RIGHT:
			right = false;
			break;
		case KeyEvent.VK_SPACE:
			if (c.form == Form.FLIGHT)
				c.changeForm(Form.NORMAL);
			break;
		}
	}
	public void keyTyped(KeyEvent e) {}
	public void mouseClicked(MouseEvent e) {
		if(state == State.GAME_PAUSED && e.getClickCount() == 2) {
			state = State.GAME_ON;
		}
	}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
}