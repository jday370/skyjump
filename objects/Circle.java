package objects;

import static misc.Constants.SIZE;

import java.awt.Color;
import java.awt.Graphics;

import main.SJGame;

public class Circle {
	private double x, startX, xvelocity, y, startY, yvelocity;
	
	public static double NORMAL_GRAVITY = 2.5;
	public double gravity = NORMAL_GRAVITY;

	public static final int NORMAL_VELOCITY_Y = 20;
	private int maxVelocityX = 45, maxVelocityY = NORMAL_VELOCITY_Y, diameter = 16;

	public double hp = 100D, power = 100D;
	// TODO when eating food diameter increases
	public boolean falling = true, ghost;

	public Form form = Form.NORMAL;
	public enum Form {
		NORMAL, MEGA, FLIGHT
	}

	public Circle() {
		x = startX = SIZE.width/2-diameter/2;
		y = startY = SIZE.height-350;
	}
	public void process() {
		move();
		if (power == 0 && form != Form.NORMAL) {
			changeForm(Form.NORMAL);
		} else {
			if (form == Form.NORMAL && power < 100)
				power += 0.5;
			else if (form != Form.NORMAL) {
				power -= 5;
				if (power < 0) power = 0;
			}
		}
		if (hp < 100)
			hp += 0.1;
	}
	public void changeForm(Form f) {
		if (f == Form.FLIGHT) {

		}
		switch (f) {
		case MEGA:
			maxVelocityY = 60;
			break;
		case FLIGHT:
			if (power < 10) return;
			maxVelocityY = NORMAL_VELOCITY_Y/2;
			break;
		default:
			maxVelocityY = NORMAL_VELOCITY_Y;
			break;
		}
		form = f;
	}
	public void toggleForm(Form f) {
		if (form == f)
			changeForm(Form.NORMAL);
		else
			changeForm(f);
	}
	public void jump() {
		falling = false;
		yvelocity = maxVelocityY;
	}
	public void reset() {
		x = startX;
		y = startY;
		yvelocity = 0D;
		xvelocity = 0D;
		hp = 100D;
		power = 100D;
	}
	public void move() { moveX(); moveY(); }
	private void moveX() {
		double power = NORMAL_GRAVITY;
		if (SJGame.left) {
			xvelocity -= power;
		}
		if (SJGame.right) {
			xvelocity += power;
		}
		if (!SJGame.left && !SJGame.right) {
			if (xvelocity < 0D)
				if (xvelocity+power > 0D)
					xvelocity = 0D;
				else
					xvelocity += power;
			else if (xvelocity > 0D)
				xvelocity -= power;
		}
		if (xvelocity > maxVelocityX)
			xvelocity = maxVelocityX;
		else if (xvelocity < -maxVelocityX)
			xvelocity = -maxVelocityX;

		if (form != Form.MEGA && Math.abs(xvelocity) >= maxVelocityX) ghost = true;
		else if (ghost) ghost = false;

		moveTo(x+xvelocity, y);
	}
	public double debug = 0;
	private void moveY() {
		if (!falling) {
			if (yvelocity > 0D)
				yvelocity = -yvelocity-gravity;
			else if (yvelocity == 0D)
				falling = true;
		}
		if (yvelocity+gravity > maxVelocityY)
			yvelocity = maxVelocityY;
		else
			yvelocity += gravity;

		if (form == Form.FLIGHT) {
			if (yvelocity-3 < -maxVelocityY)
				yvelocity = -maxVelocityY;
			else
				yvelocity -= 3;
		}
		if (yvelocity > 0D) falling = true;
		else if (yvelocity < 0D) falling = false;

	//	if (yvelocity < 0D) debug-=yvelocity;
	//	System.out.println(debug);
	//	if (yvelocity == 0D) System.exit(0);
	//	System.out.println();
	//	System.out.println(20*(20D/gravity)-gravity*(20D/gravity)*2); // 89
		moveTo(x, y+yvelocity);
	}
	public void moveTo(final double xLoc, final double yLoc) {
		while ((int)x != (int)xLoc) {
			if (x > xLoc) 
				x--;
			else 
				x++;
			SJGame.checkHit(this);
		}
		while ((int)y != (int)yLoc) {
			if (y > yLoc) 
				y--;
			else 
				y++;
			SJGame.checkHit(this);
		}
	}
	public void teleTo(final double xLoc, final double yLoc) {
		x = xLoc;
		y = yLoc;
		SJGame.checkHit(this);
	}
	public void paint(Graphics g) {
		// TODO red health potions
		// TODO battles, red nemesis, magenta victim
		switch (form) {
		case NORMAL:		
			g.setColor(Color.white);
		g.fillOval((int)x-1, (int)y-1, diameter+2, diameter+2);
			g.setColor(Color.green);
			g.fillOval((int)x, (int)y, diameter, diameter);
			break;
		case MEGA:
			g.setColor(Color.blue);
			g.fillOval((int)x, (int)y, diameter, diameter);
			g.setColor(Color.white);
			g.drawOval((int)x, (int)y, diameter, diameter);
			break;
		case FLIGHT:
			g.setColor(Color.white);
			g.fillOval((int)x-2, (int)y-2, diameter+4, diameter+4);
			g.setColor(Color.yellow);
			g.fillOval((int)x, (int)y, diameter, diameter);
			break;
		}
	}
	public int getRadius() { return diameter/2; }
	public int getDiameter() { return diameter; }
	public double getX() { return x; }
	public void setX(double xx) { x = xx; }
	public double getY() { return y; }
	public void setY(double yy) { y = yy; }
	public double getXVelocity() { return xvelocity; }
	public double getYVelocity() { return yvelocity; }
	public double getMaxVelocityY() { return maxVelocityY; }
	public double getMaxVelocityX() { return maxVelocityX; }
	public static int getMaxJumpHeight() {
		final int n = (int) (NORMAL_VELOCITY_Y/NORMAL_GRAVITY);
		return -(int)((n/2)*(-NORMAL_VELOCITY_Y*2+(n-1)*NORMAL_GRAVITY));
	}
}
