package objects;

import java.awt.Color;
import java.awt.Graphics;

public class FragileBlock extends Block {

	public FragileBlock(int x, int y) { super(x, y); }
	public void paint(Graphics g) {
		g.setColor(Color.gray);
		g.fillRect(x, y, WIDTH, HEIGHT);
		g.setColor(Color.darkGray);
		g.fillRect(this.x+(WIDTH-28)/2, this.y, 28, 3);
		g.setColor(Color.black);
	}
}
