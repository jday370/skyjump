package objects;

import java.awt.Color;
import java.awt.Graphics;

import misc.Methods;

public class Block {
	protected int x, y;
	protected static final int WIDTH = 50, HEIGHT = 8;

	public boolean touched = false;
	public Block(final int x, final int y) {
		this.x = x;
		this.y = y;
	}
	public void moveDown(final int speed) {
		y += Math.abs(speed);
	}
	public void paint(final Graphics g) {
		g.setColor(Color.gray);
		g.fillRect(x, y, WIDTH, HEIGHT);
		if (touched)
			g.setColor(Color.green);
		else
			g.setColor(Color.red);
		g.fillRect(this.x+(WIDTH-28)/2, this.y, 28, 3);
		g.setColor(Color.black);
	}
	public boolean intersects(final int x, final int y, final int w, final int h) {
		return Methods.intersects(x, y, w, h, this.x+(WIDTH-28)/2, this.y, 28, 3);
	}
	public static int getWidth() { return WIDTH; }
	public static int getHeight() { return HEIGHT; }
	public int getX() { return x; }
	public int getY() { return y; }
}
