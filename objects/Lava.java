package objects;

import static misc.Constants.SIZE;

import java.awt.Color;
import java.awt.Graphics;

import misc.Methods;

public class Lava {
	private int y, height;

	public Lava() {
		reset();
	}
	public void reset() {
		y = SIZE.height;
	}
	public void moveUp(final int speed) {
		if (y > 0)
			y -= Math.abs(speed);
	}
	public void moveDown(final int speed) {
		y += Math.abs(speed);
	}
	public boolean isOnScreen() {
		return y < SIZE.height;
	}
	private Color lavaRed = new Color(200, 0, 0);
	public void paint(final Graphics g) {
		g.setColor(lavaRed);
		g.fillRect(0, y, SIZE.width, (height=SIZE.height-y));
	}
	public boolean intersects(final int x, final int y, final int w, final int h) {
		return Methods.intersects(x, y, w, h, 0, this.y, SIZE.width, height);
	}
	public static int getWidth() { return SIZE.width; }
	public int getHeight() { return height; }
	public int getX() { return 0; }
	public int getY() { return y; }
}
