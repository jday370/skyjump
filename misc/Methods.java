package misc;

import java.util.Random;

public class Methods {
	
	private static final Random random = new Random();
	public static int randInt(int min, int max) {
		return min + random.nextInt(max-min);
	}
	public static boolean intersects(int x, int y, int w, int h, int x2, int y2, int w2, int h2) {
		return ((x + w > x2) && (y + h > y2) && (x < x2 + w2) && (y < y2 + h2));
	}
}
