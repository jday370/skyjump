package misc;

import java.awt.Dimension;

public class Constants {
	public static final double VERSION = 0.8;
	public static final Dimension SIZE = new Dimension(800, 600);

	public static final int START_BLOCKS = 100;
}
