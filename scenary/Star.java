package scenary;

import static misc.Constants.SIZE;
import static misc.Methods.randInt;

import java.awt.Color;
import java.awt.Graphics;

public class Star {

	 int x, y, side;
	Star() {
		respawn();
	}
	void respawn() {
		x = newX();
		y = newY();
		final int random = randInt(0, 6);
		if (random >= 0 && random < 3)
			side = 3;
		else side = random;
	}
	private int[] attempts = { 0, 0 }; //debugging variable
	private int newX() {
		final int newX = randInt(0, SIZE.width);
		for (Star star : StarGroup.stars)
			if (newX-10 <= star.x && newX+10 >= star.x) {
				if (attempts[0] > 10) break;
				attempts[0]++;
				return newX();
			}
		attempts[0] = 0;
		return newX;
	}
	private int newY() {
		final int newY = randInt(-SIZE.height, 10);
		for (Star star : StarGroup.stars)
			if (newY-10 <= star.y && newY+10 >= star.y) {
				if (attempts[1] > 10) break;
				attempts[1]++;
				return newY();
			}
		attempts[1] = 0;
		return newY;
	}
	void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(x, y, side, side);
	}
}
