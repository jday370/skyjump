package scenary;

import static misc.Constants.SIZE;

import java.util.ArrayList;
import java.util.List;

public class StarGroup {

	static List<Star> stars = new ArrayList<Star>();
	static int starLimit = 0;

	static void process() {
		if (starLimit != 0) {
			if (starLimit > stars.size())
				stars.add(new Star());
			for (int i=0; i<stars.size(); i++) {
				final Star star = stars.get(i);
				if (star.y >= SIZE.height) {
					if (starLimit > stars.size())
						star.respawn();
					else if (starLimit < stars.size()) {
						stars.remove(star);
						i--;
					}
				}
			}
		}
	}
	public static void moveDown(final int speed) {
		for (Star star : stars) {
			star.y += Math.abs(speed/2);
		}
	}
}
