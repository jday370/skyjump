package scenary;

import static misc.Methods.randInt;
import static scenary.StarGroup.stars;

import java.awt.Graphics;

public class ScenaryHandler {

	public static void process(final double height) {
		StarGroup.process();

		if (height >= 255) { // black night
			StarGroup.starLimit = randInt(50, 75);
		}
	}
	public static void reset() {
		stars.clear();
		StarGroup.starLimit = 0;
	}
	public static void moveDown(final int speed) {
		StarGroup.moveDown(speed);
	}
	public static void paint(Graphics g) {
		for (Star star : stars)
			star.paint(g);
	}
}
